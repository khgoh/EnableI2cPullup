/*
 * To compile the code
 * gcc EnableI2cPullup.c -o EnableI2cPullup
 *
 * Make sure the program is run under root privilage
 * sudo ./EnableI2cPullup
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>


/*
 * GPIO Base Address for Allwinner CPU
 */
#define SUNXI_GPIO_BASE0 (0x01C20800)
#define SUNXI_GPIO_BASE1 (0x01F02C00)
/* 
 * Offset address for PH wher I2c1 is located
 * pull up/down value
 * 0 = off
 * 1 = Pull up
 * 2 = Pull down
 */
#define PHPULL0 ((7*0x24)+0x1C)
#define PCPULL0 ((2*0x24)+0x1C)
#define PLPULL0 ((0*0x24)+0x1C)
 
#define PAGE_SIZE (4*1024)
 
volatile unsigned *gpio0;
volatile unsigned *gpio1; 

// Setup the I/O memory map
void setup_io(int gpiono)
{
	int  mem_fd;
	void *gpio_map;
	unsigned int  addr_start, addr_offset, PageSize, PageMask;
	PageSize = PAGE_SIZE;
	PageMask = (~(PageSize-1));
	if (gpiono==0) {
		addr_start = SUNXI_GPIO_BASE0 & PageMask;
		addr_offset = SUNXI_GPIO_BASE0 & ~PageMask;
	}
	else {
		addr_start = SUNXI_GPIO_BASE1 & PageMask;
		addr_offset = SUNXI_GPIO_BASE1 & ~PageMask;
	}
	 
   /* open /dev/mem */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem \n");
      exit(-1);
   }
 
   /* mmap GPIO */
   gpio_map = mmap(
      NULL,             //Any adddress in our space will do
      PageSize,       //Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       //Shared with other processes
      mem_fd,           //File to map
      addr_start         //Offset to GPIO peripheral
   );
 
   close(mem_fd); //No need to keep mem_fd open after mmap
 
   if (gpio_map == MAP_FAILED) {
      printf("mmap error %d\n", errno);//Print errno
      exit(-1);
   }
 
   // Always use volatile pointer!
   if (gpiono==0) {
	   gpio0 = (volatile unsigned *)(gpio_map+addr_offset);
	 }
	 else {
	 	 gpio1 = (volatile unsigned *)(gpio_map+addr_offset);
	 }
}

// Read from the GPIO
uint32_t readgpio(int gpiono, uint32_t offset)
{
	volatile uint32_t *addr;
	if (gpiono==0) {
		addr=(volatile uint32_t *)((uint64_t)gpio0+offset);
	}
	else {
		addr=(volatile uint32_t *)((uint64_t)gpio1+offset);
	}
	//printf("Reading: gpiobase %08lx, offset %04x, final addr %08lx\r\n",(uint64_t)(gpio),offset,(uint64_t)(addr));
	return(*(addr));
}

// Write into GPIO
void writegpio(int gpiono, uint32_t offset,uint32_t data)
{
	volatile uint32_t *addr;
	if (gpiono==0) {
		addr=(volatile uint32_t *)((uint64_t)gpio0+offset);
	}
	else {
		addr=(volatile uint32_t *)((uint64_t)gpio1+offset);
	}
	*addr=data;
	//printf("Writing: gpiobase %08lx, offset %04x, final addr %08lx writedata %04x, rereaddata %04x\r\n",(uint64_t)(gpio),offset,(uint64_t)(addr),data,(*addr));
}

#define bitno(pin) ((pin)*2)

int main (int argc, char *argv []){
	uint32_t tmp;
	setup_io(0);	//setup the io
	setup_io(1);	//setup the io
	
	tmp=readgpio(0,PHPULL0);									//read from the gpio register
	tmp &= ~((3<<bitno(2))|(3<<bitno(3))|(3<<bitno(5))|(3<<bitno(6)));    //mask off the bit 2 and 3
	tmp |=((1<<bitno(2))|(1<<bitno(3))|(1<<bitno(5))|(1<<bitno(6)));		  //set bit 2 and 3 to enable pull up.
	writegpio(0,PHPULL0,tmp); 								//write the setting into the gpio register
	
	tmp=readgpio(0,PCPULL0);	
	tmp &= ~((3<<bitno(5))|(3<<bitno(9)));
	tmp |=((1<<bitno(5))|(1<<bitno(9)));
	writegpio(0,PCPULL0,tmp);
	
	tmp=readgpio(1,PLPULL0);	
	tmp &= ~((3<<bitno(8))|(3<<bitno(9)));
	tmp |=((1<<bitno(8))|(1<<bitno(9)));
	writegpio(1,PLPULL0,tmp);
	printf("I/O Port for Pine64 I2c POT pull up enabled.\n\r");
	return(0);
}